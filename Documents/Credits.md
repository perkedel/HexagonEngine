# Credits
Big Shoutout!
## Refferences
Table of Refferences!
|Author	|Title	|Description	|Source	|Rights Reserved	|License	|Raw Credit	|
|-------|--------|--------------|-------|:-----------------:|-----------|-----------|
|adam-p	|Markdown cheatsheet	|List of syntax of Markdown| https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet	| <100%	|	CC-BY	|`-`|
|Juan Linietsky & friends	|Godot Signal| Connect Signal|https://docs.godotengine.org/en/3.1/getting_started/step_by_step/signals.html | <100%| CC3.0-BY|`© Copyright 2014-2019, Juan Linietsky, Ariel Manzur and the Godot community (CC-BY 3.0)`|
|Juan Linietsky & friends	|Godot Script Variables| Export the variables to make is visible in inspector|https://docs.godotengine.org/en/latest/getting_started/scripting/gdscript/gdscript_basics.html#exports | <100%| CC3.0-BY|`© Copyright 2014-2019, Juan Linietsky, Ariel Manzur and the Godot community (CC-BY 3.0)`|
|GDquest|Custom Node Warning|Add Custom Warning in the Node for missing thing| https://www.youtube.com/watch?v=90XPxd2533c | <100%|???| `-`|
|GDquest|Title Screen|How to make main menu and connect to scenes| https://youtu.be/sKuM5AzK-uA | <100%|???| `-`|
|Zylann and friends|remove child and add child|Go to next level without removing entire node| https://godotengine.org/qa/24773/how-to-load-and-change-scenes | <100%|???| `-`|
|Zylann and friends|add child in specific position|add child node with its specific position| https://godotengine.org/qa/8025/how-to-add-a-child-in-a-specific-position | <100%|???| `-`|
|D-ring and friends|Swipe Scroll|Scroll with swipe for both mouse and touch| https://godotforums.org/discussion/20206/swipe-function-with-scroll | <100%|???| `-`|
|avencherus|Grab Focus|Grab the navigation focus| https://godotforums.org/discussion/20206/swipe-function-with-scroll | <100%|???| `-`|
|Juan Linietsky & friends	|String Format| Format String for dynamic dialogue application|https://docs.godotengine.org/en/3.1/getting_started/scripting/gdscript/gdscript_format_string.html | <100%| CC3.0-BY|`© Copyright 2014-2019, Juan Linietsky, Ariel Manzur and the Godot community (CC-BY 3.0)`|
|Juan Linietsky & friends	|Pause Game| Using paused tree and its whitelist to nodes|https://docs.godotengine.org/en/latest/tutorials/misc/pausing_games.html | <100%| CC3.0-BY|`© Copyright 2014-2019, Juan Linietsky, Ariel Manzur and the Godot community (CC-BY 3.0)`|
|GDquest|Pause game Tutorial|How to make pause game| https://www.youtube.com/watch?v=Jf7F3JhY9Fg | <100%|???| `-`|
|lukas|Quit|GD script quit| https://godotengine.org/qa/554/is-there-a-way-to-close-a-game-using-gdscript | <100% | ??? | `-`|
|Calinou| recedit | Set icon in Windows | https://godotengine.org/qa/31470/how-to-set-icon-in-app | <100% | ??? | `-` |
|Juan Linietsky & friends	|Custom Cursor | Implement own game cursor|https://docs.godotengine.org/en/3.1/tutorials/inputs/custom_mouse_cursor.html | <100%| CC3.0-BY|`© Copyright 2014-2019, Juan Linietsky, Ariel Manzur and the Godot community (CC-BY 3.0)`|
|Alex Horatio| Popup tutorial | Types of popup node in Godot | https://www.youtube.com/watch?v=L0anvhjwdU8 | <100% | ??? | `-` |
|Calinou| GUI Follow Camera | Place all element that has to follow camera under Canvas Layer. | https://godotengine.org/qa/396/gui-not-following-camera | <100% | ??? | `-` |
|duke_meister | Quit request | Quit application heurestics | https://godotengine.org/qa/4768/android-ios-application-lifecycle | <100% | ??? | `-`|
|Mike GamesFromScratch | Keyboard, Touch, Gamepad Input | 3 Major input tutorial | https://www.gamefromscratch.com/page/Godot-3-Tutorial-Keyboard-Mouse-and-Joystick-Input.aspx | <100% | ??? | `-` |
|Juan Linietsky & friends	|Background Loading| Threaded Loading|https://docs.godotengine.org/en/3.1/tutorials/io/background_loading.html | <100%| CC3.0-BY|`© Copyright 2014-2019, Juan Linietsky, Ariel Manzur and the Godot community (CC-BY 3.0)`|
|Brackeys| Unity Loading Bar| using Coroutine of an IEnumerator progress watching and spawn of Scene| https://www.youtube.com/watch?v=YMj2qPq9CP8 | <100% | ??? | `-`|
|Perkedel Technologies| Demo of Brackeys Loading Bar but it's Godot instead of Unity | How did Joel make his own version of Loading Resource Scene with Loading Bar for Godot Engine| https://github.com/Perkedel/Where_Is_LoadingBar_Functionality/blob/master/GenLoad.gd | <100% | GNU GPL v3 | `by JOELwindows7; Perkedel Technologies; GNU GPL v3; Some Rights Reserved`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
|<*Author name*> u|<*Title*> h|<*Description*> h|https://example.com |0%|CC0|`-`|
## Assets and Tools
Used Assets and Tools!
|Author	|Title	|Description	|Source	|Rights Reserved	|License	| Raw Credit	|
|-------|-------|---------------|-------|:-----------------:|-----------|---------------|
|Juan Linietsky and Core Contributors|Godot Engine|Open Source, Gratis, Full, Libre, free beer and freedom game engine, AAA capable!!!|https://godotengine.org |<100%|MIT|`-` |
|Inkscape Community|Inkscape|Vector graphic drawing software| https://inkscape.org | <100% | GNU GPL v2+ | `Inkscape is Free and Open Source Software licensed under the GPL. `|
|Ton Roosendaal, Blender Foundation| Blender | Full fledged 3D tool suite in one software | https://blender.org | <100% | GNU GPL v2+ | `-` |
|Audacity Team | Audacity | All in one Non-linear Audio editing software | https://www.audacityteam.org/ | <100% | GNU GPL v2 | `uhhh help rights reserved wronged to "ALL" instead of "SOME"`|
|electron | rcedit | hack Windows exe resource | https://github.com/electron/rcedit/releases | <100% | MIT | `-` |
|GIMP team | GIMP | Raster image editing software | https://www.gimp.org/ | <100% | GNU GPL v2+ | `-` |
|badges | Badge on GitHub Custom | Make Own Badge for GitHub | https://www.shields.io/ | 0% | CC0-1.0 | `-` |
|dwyl | Hits | Track Hit view count of your GitHub Git | https://hits.dwyl.com/ | <100% | ??? | `-` |
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
|<*Author name*>	|<*Title*>	|<*Description*>	|<*Source*>	|<*Rights Reserved*>	|<*License*>	| <*Raw Credit*>	|
## Unimplemented Credit, QUick And RAW reference
- https://godotengine.org/qa/60870/how-do-i-change-datatype-of-a-signal-using-gdscript
- https://www.youtube.com/watch?v=o77wFWau9Wc
- https://www.youtube.com/watch?v=AkfTW2Tq3MM
- https://www.youtube.com/watch?v=q4MdQ5c-NIY
- https://freesound.org/people/qubodup/sounds/60007/
- https://freesound.org/people/moogy73/sounds/425728/
- https://www.youtube.com/audiolibrary
- Search "Floaters" artist is "Jimmy Fontanez"
- https://godotengine.org/qa/24773/how-to-load-and-change-scenes
- https://godotengine.org/qa/8025/how-to-add-a-child-in-a-specific-position 
- https://godotengine.org/qa/24773/how-to-load-and-change-scenes
- https://github.com/Shin-NiL/Godot-Android-Admob-Plugin
- https://github.com/Shin-NiL/Godot-Android-Admob-Plugin/releases
- https://github.com/kloder-games/godot-admob
- https://adoptopenjdk.net/
- https://www.oracle.com/java/technologies/javase-downloads.html
- https://stackoverflow.com/questions/5730815/unable-to-locate-tools-jar
- https://docs.godotengine.org/en/latest/getting_started/workflow/export/android_custom_build.html
- https://developers.google.com/admob/android/test-ads
- https://github.com/electron/rcedit/releases
- https://casual-effects.com/data/
- Models downloaded from Morgan McGuire's [Computer Graphics Archive](https://casual-effects.com/data)
- https://www.fontsquirrel.com/fonts/oswald
- https://dbarchive.biosciencedbc.jp/en/bodyparts3d/download.html
- http://archive.zbrushcentral.com/showthread.php?169189
- https://blenderartists.org/t/free-full-anatomy-model/543683
- https://www.deviantart.com/krypt77/art/Blender-s-candies-655626371
- https://www.3dcadbrowser.com/3dmodels.aspx?download=organs
- https://www.blender.org/forum/viewtopic.php?t=18604
- http://godotengine.org/qa/11538/get-mouse-position-in-world
- https://m.box.com/shared_item/https%3A%2F%2Fapp.box.com%2Fs%2Fz14r2tofxp
- https://www.blender.org/forum/viewtopic.php?t=24068
- http://godotengine.org/qa/130/how-can-detect-collision-from-collisionpollygon2d-object
- http://docs.godotengine.org/en/latest/classes/class_rigidbody2d.html#class-rigidbody2d-property-contacts-reported
- http://godotengine.org/qa/130/how-can-detect-collision-from-collisionpollygon2d-object
- http://docs.godotengine.org/en/latest/tutorials/physics/physics_introduction.html
- https://twitter.com/Maddnsk/status/1229817927521767427?s=09
- https://docs.godotengine.org/en/3.1/tutorials/2d/particle_systems_2d.html 
- http://godotengine.org/qa/6615/dragging-object-in-godot-rigidbody2d-dragging
- https://www.reddit.com/r/okbuddyretard/comments/erhnfb/funky/
- http://godotengine.org/qa/15528/how-do-i-delete-my-node-once-im-done-with-it
- http://kidscancode.org/blog/2017/04/godot_101_12/
- http://godotengine.org/qa/27280/button-over-monitored-area-mouse-filter
- http://godotengine.org/qa/14699/how-do-i-spawn-objects-in-the-world-at-specific-places
- http://godotengine.org/qa/33634/connect-a-click-event-not-in-canvaslayer
- https://docs.godotengine.org/en/3.1/getting_started/workflow/assets/import_process.html
- http://godotengine.org/qa/27280/button-over-monitored-area-mouse-filter
- http://godotengine.org/qa/40998/how-to-make-an-infinite-scrolling-background
- https://docs.godotengine.org/en/3.1/tutorials/2d/particle_systems_2d.html
- http://godotengine.org/qa/13845/canvas-layer-and-input-events
- https://www.deviantart.com/nitwitsworld/art/Breezie-model-by-mrokidoki97-773367507
- http://godotengine.org/qa/15528/how-do-i-delete-my-node-once-im-done-with-it
- https://github.com/fenix-hub/godot-engine.text-editor
- https://github.com/godot-extended-libraries
- Guys! Please, put your credit file in the each of your addon folder!
- https://github.com/fenix-hub/godot-engine.github-integration/wiki/Installation-&-Usage
- https://help.github.com/en/github/authenticating-to-github/creating-a-personal-access-token-for-the-command-line
- https://github.com/Wavesonics/LANServerBroadcast
- https://github.com/thomazthz/godot-wakatime
- https://github.com/ghsoares/dynamic_water_plugin
- https://wakatime.com/developers/
- https://github.com/zcaliptium/gdinv
- https://github.com/zcaliptium/gddlg
- https://godotengine.org/asset-library/asset/182
- https://godotengine.org/asset-library/asset/522
- https://godotengine.org/asset-library/asset/240
- https://bitbucket.org/arlez80/godot-midi-player/src/master/
- https://github.com/dploeger/godot-scrollingbackground/blob/master/addons/scrolling_background/README.md
- https://github.com/dploeger/godot-scrollingbackground
- https://github.com/sketchfab/godot-plugin
- https://github.com/godot-extended-libraries/godot-ideas
- https://github.com/godot-extended-libraries/godot-mat-cap/blob/master/LICENSE.md
- https://github.com/GDQuest/godot-3d-mannequin
- https://github.com/fenix-hub?tab=repositories
- https://github.com/fenix-hub
- https://github.com/fenix-hub/godot-engine.github-integration/tree/master/addons/github-integration
- https://github.com/godot-extended-libraries/godot-sqlite
- https://github.com/godot-extended-libraries/godot-plugin-refresher
- https://github.com/fenix-hub/godot-engine.text-editor
- https://github.com/godot-extended-libraries
- https://kenney.nl/
- https://armory3d.org/demo/twin_stick/
- https://github.com/Wavesonics/LANServerBroadcast
- https://github.com/marketplace/actions/godot-export
- https://help.github.com/en/actions/configuring-and-managing-workflows/configuring-a-workflow
- https://docs.godotengine.org/en/latest/getting_started/workflow/export/exporting_for_uwp.html
- https://github.com/Malinskiy/action-android
- https://github.com/JOELwindows7/ourwear_prototype
- https://github.com/marketplace/actions/github-script
- https://stackoverflow.com/questions/4823468/comments-in-markdown
- https://daringfireball.net/projects/markdown/syntax#link
- https://circleci.com/docs/2.0/status-badges/
- https://nakyle.com/godot-ci/
- https://circleci.com/docs/2.0/docker-layer-caching/
- https://circleci.com/docs/2.0/docker-layer-caching/
- https://youtu.be/39_HhRkXDpQ Poop post explosion meme sound original
- https://sketchfab.com/3d-models/cuphead-cb9d5b97130141f98e0daccd41e0deb7
- https://sketchfab.com/3d-models/epson-wacom-laptop-c8b1de9e6b05449f9f01cc803cf70797
- https://sketchfab.com/3d-models/godette-chan-5e5dd8978e21431f992dee953c11558d
- https://github.com/vini-guerrero/Godot_Game_Tools **WHAT THE PECK?! WHERE IS THE ORIGINAL FBX & BLEND FILE?! WHY ONLY HERE THIS? HUH? AM i MISSING SOMETHING?!**
- https://github.com/WolfgangSenff/GodotFirebase/
- https://github.com/Shin-NiL/Godot-Android-Admob-Plugin
- https://github.com/yalcin-ata/godot-android-plugin-firebase
- https://docs.godotengine.org/en/stable/classes/class_file.html
- https://skfb.ly/6SvpG
- https://www.javatpoint.com/queue-free-and-reload-current-scene-functions-in-godot
- https://ash-k.itch.io/textreme-2 **HOW DARE YOU?!**
- https://itch.io/t/929224/code-editor-by-yafd **actually he's a nice guy?**
- https://github.com/maksloboda/Godot-Tutorials **my apology**
- https://github.com/jotson/ridiculous_coding **OH!!!**
- a
- https://godotengine.org/asset-library/asset/157
- https://gamefromscratch.com/vpainter-for-godot/
- https://github.com/tomankirilov/VPainter
- https://github.com/godotengine?page=1
- https://github.com/godotengine/godot
- https://github.com/godotengine/awesome-godot
- https://github.com/aBARICHELLO/godot-ci/blob/master/.github/workflows/godot-ci.yml#L8-99
- https://github.com/utopia-rise/fmod-gdnative **FMOD plugin GDNative**
- https://generalistprogrammer.com/godot/godot-for-loop-tutorial-definitive-guide-with-examples/
- https://generalistprogrammer.com/godot/godot-dictionary-tutorial-with-examples/
- https://www.fmod.com **PROPRIETARY AMBIGUOUS-GRATIS!!! DO NOT REDISTRIBUTE WITH SOURCE CODE; IMPLEMENTATION CANCELED, WILL LOOK AGAIN LATER**
- https://github.com/marketplace/actions/build-godot 
- https://godotengine.org/community
- https://github.com/godotengine/awesome-godot
- https://anti.itch.io/super-tux-party **Fork for DVD making!!!**
- https://docs.godotengine.org/en/stable/classes/class_tree.html#class-tree
- https://docs.godotengine.org/en/stable/getting_started/workflow/export/exporting_for_uwp.html
- https://docs.microsoft.com/id-id/windows/win32/appxpkg/how-to-create-a-package-signing-certificate?redirectedfrom=MSDN
- https://docs.microsoft.com/en-us/windows/msix/package/create-certificate-package-signing
- https://docs.microsoft.com/en-us/powershell/module/pkiclient/New-SelfSignedCertificate?view=win10-ps
- https://duckduckgo.com/?q=makecert+fail&t=brave&ia=web&iai=r1-0&page=1&adx=prdsdc&sexp=%7B%22prodexp%22%3A%22b%22%2C%22prdsdexp%22%3A%22c%22%2C%22biaexp%22%3A%22b%22%2C%22direxp%22%3A%22b%22%7D
- https://stackoverflow.com/questions/9506671/why-do-i-keep-getting-a-failed-when-trying-to-make-a-cer-for-testing
- https://docs.microsoft.com/id-id/powershell/scripting/install/installing-powershell-core-on-windows?view=powershell-7
- https://github.com/PowerShell/PowerShell/releases/tag/v7.0.3
- **here are the MIDI related stuffs**
- http://xg-central.com/xgc-introduction-midi.php
- They have
- GM On F0 7E 7F 09 01 F7
- XG On F0 43 10 4C 00 00 7E 00 F7
- https://professionalcomposers.com/midi-cc-list/
- https://studiocode.dev/resources/midi-pitch-bend/
- https://www.recordingblogs.com/wiki/midi-registered-parameter-number-rpn
- http://www.2writers.com/eddie/TutNrpn.htm#:~:text=The%20MIDI%20Specification%20currently%20defines,Coarse%20Tuning%20(0%2C2)
- https://stackoverflow.com/questions/58622317/is-it-possible-to-change-android-built-in-midi-player-pitch-bend-range
- https://quadrophone.com/synthesis/using-pitch-bend/
- https://www.all8.com/tools/bpm.htm
- http://www.beatsperminuteonline.com/
- https://www.manualslib.com/manual/196284/Yamaha-Portable-Grand-Dgx-305.html?page=97#manual
- https://www.tobias-erichsen.de/software/loopmidi.html
- **ENd of MIDI articles**
- https://www.youtube.com/watch?v=9iUUgTS-ndw
- https://www.youtube.com/watch?v=Yiew9ru-6a0
- https://youtu.be/Yiew9ru-6a0
- https://reddit.com/r/Shantae3DModel (Not Suitable for Wumpus) (The harsh Chapter)
- https://github.com/Perkedel/After-Church/blob/master/RAW%20files/Blender/unMixamoStick.blend (The harsh chapter)
- https://github.com/Perkedel/After-Church/blob/master/RAW%20files/Blender/ZoblatosImprovement.blend
- https://godotengine.org/qa/76823/add-a-sound-to-all-the-buttons-in-a-project
- https://github.com/godotengine/godot-proposals/issues/1472
- https://github.com/godotengine/godot/issues/3608
- https://gamedev.stackexchange.com/questions/184354/add-a-sound-to-all-the-buttons-in-a-project/184363#184363 
- https://shkspr.mobi/blog/2015/07/exporting-multitrack-surround-files-in-audacity/
- https://github.com/Arnklit/ShellFurGodot
- https://godotengine.org/asset-library/asset/916 & https://github.com/gilzoide/godot-dockable-container Godot dockable Container
- https://godotengine.org/asset-library/asset/460 loading async brackey-like
- https://www.git-tower.com/learn/git/faq/git-add-remote/ add git remote